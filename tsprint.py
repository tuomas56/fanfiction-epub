from multiprocessing import Lock

#Thread-safe print function.

mylock = Lock()
p = print

def print(*a, **b):
	with mylock:
		p(*a, **b)
