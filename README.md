fanfiction-epub
===============


```fanfiction-epub``` converts fanfictions from various websites to 
EPUBv3 ebooks. It is written in Python 3.

Supported Websites
-----------------


* fanfiction.net

CLI Usage
--------

```ffdl.py [-o FILE_NAME | -l] URL```


Option|Meaning
------|--------
-o FILE|Output URL as an EPUB at FILE.|
-l     |URL is a list of fanfics, seperated by newlines. Download them all and save them each to an appropriate filename. |

